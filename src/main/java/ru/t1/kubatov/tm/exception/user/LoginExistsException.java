package ru.t1.kubatov.tm.exception.user;

import ru.t1.kubatov.tm.exception.field.AbstractFieldException;

public final class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
