package ru.t1.kubatov.tm.exception.field;

public class LoginExistsException extends AbstractFieldException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
